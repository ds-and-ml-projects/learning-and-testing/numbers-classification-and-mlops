#!/bin/bash

mkdir -p "./dataset"
mkdir -p "./docs"
mkdir -p "./models"
mkdir -p "./notebooks"
mkdir -p "./reports"
mkdir -p "./src"

mkdir -p "./dataset/raw"
mkdir -p "./dataset/initial"
mkdir -p "./dataset/external"
mkdir -p "./dataset/interim"
mkdir -p "./dataset/processed"

mkdir -p "./src/data"
mkdir -p "./src/models"
mkdir -p "./src/features"
mkdir -p "./src/visualization"

echo "Folder structure for ML Project has been created"